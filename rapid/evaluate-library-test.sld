;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid evaluate-library-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid assume)
	  (rapid set)
	  (rapid mapping)
	  (rapid syntax)
	  (rapid syntactic-environment)
	  (rapid primitives)
	  (rapid analyze-library)
	  (rapid expand-library)
	  (rapid evaluate-library))
  (begin
    (define (run-tests)
    
      (test-begin "R7RS library evaluator")

      (parameterize
	  ((library-exports-accessor test-library-exports)
	   (library-dependencies-accessor test-library-dependencies)
	   (library-environment-accessor test-library-environment)
	   (library-store-accessor test-library-store))

	(define-values (exports imports dependencies body)
	  (analyze-library
	   '(rapid)
	   (unwrap-syntax
	    (syntax
	     ((import (rapid primitive))
	      (begin
		(define-values (foo) 42)))))))
	
	(define-values (environment definitions)
	  (expand-library '(rapid) exports imports body))

	(define-values (store)
	  (evaluate-library dependencies definitions))

	(define (lookup identifier)
	  (parameterize ((current-syntactic-environment environment))
	    (denotation-location (binding-denotation (lookup-binding! identifier)))))
	
	(test-equal 42 (cadr (assq (lookup 'foo) store))))    
      
      (test-end))
    
    (define (test-library-dependencies library-name)
      (cond
       ((equal? library-name '(rapid primitive))
	(set library-name-comparator))))
    
    (define (test-library-exports context library-name)
      (cond
       ((equal? library-name '(rapid primitive))
	rapid-primitive-exports)
       (else
	(assume #f))))

    (define (test-library-environment library-name)
      (cond
       ((equal? library-name '(rapid primitive))
	rapid-primitive-environment)
       (else
	(assume #f))))
    
    (define (test-library-store library-name)
      (cond
       ((equal? library-name '(rapid primitive))
	rapid-primitive-store)
       (else
	(assume #f))))))
