;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (evaluate-library dependencies definitions)
  (let*-values
      (((store)
	(set-fold (lambda (dependency store)
		    (append (library-store dependency) store))
		  '() dependencies))
       ((locations)
	(fold (lambda (definition locations)
		(syntax-match definition
		  ((define-values (,(unwrap-syntax -> arg1*) ... .
				   ,(unwrap-syntax -> arg2*)) ,expression)
		   ;; FIXME: ,@arg2* may be the empty list. Logical error.
		   `(,@arg1* ,@arg2* . ,locations))))
	      '() definitions))
       ((expression)
	`(lambda (,@(map car store))
	   ,@(map syntax->datum definitions)
	   (values ,@locations)))
       (objects
	(apply (eval-primitive expression)
	       (map cadr store))))
    (map list locations objects)))

(define (library-store library-name)
  ((library-store-accessor) library-name))

;;;; Parameter objects

(define library-store-accessor
  (make-parameter (lambda (library-name)
		    (assume #f "library-store-accessor: has to be bound"))))




