;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> R7RS library evaluator.

(define-library (rapid evaluate-library)
  (export evaluate-library
	  library-store
	  library-store-accessor)
  (import (scheme base)
	  (rapid assume)
	  (rapid list)
	  (rapid set)
	  (rapid syntax)
	  (rapid syntactic-environment)
	  (rapid eval-primitive))
  (include "evaluate-library.scm"))
